<?php

// A voir si je veux séparer les fonctions en différentes class genre, individual, donation. DBS deviendrait une factory.
// DBS parent les autres extend de DBS pour avoir accès au curl. (une class peut avoir d'une autre, require). 

namespace PO;

class DataBrokerService
{
    private $apiKey;
    private $userName;
    private $userPassword;
    private $endPoint;

    public function __construct(array $params)
    {
        $this->apiKey = $params['apiKey'];
        $this->userName = $params['userName'];
        $this->userPassword = $params['userPassword'];
        $this->endPoint = $params['endPoint'];
    }

    // CURL CALL
    private function callDBS(string $url, bool $isPostRequest = false, array $callParameters = [])
    {
        $curl = curl_init();

        $curlOptions = [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->endPoint.$url,
            CURLOPT_HTTPAUTH => CURLAUTH_NTLM,
            //CURLOPT_USERPWD => "$this->userName:$this->userPassword",
            CURLOPT_HEADER => 0,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
        ];

        // Add parameters if it's a post call
        if ($isPostRequest) {
            $curlOptions[CURLOPT_POST] = count($callParameters);
            $curlOptions[CURLOPT_POSTFIELDS] = json_encode($callParameters);
            $curlOptions[CURLOPT_HTTPHEADER] = ['APIKey: '.$this->apiKey, 'Content-Type:application/json'];
        } else {
            $curlOptions[CURLOPT_HTTPHEADER] = ['APIKey: '.$this->apiKey];
        }

        curl_setopt_array($curl, $curlOptions);

        // Send the request & return the response
        //TODO ici on ne distingue pas le retour sans donnée et une erreur ds l'appel (500 ou autre)//
        // Idéal envoyer un mail si erreur et petit message "une erreur est survenue"
        $resp = curl_exec($curl);
        if (empty(json_decode($resp))) {
            return false;
        }

        return $json = json_decode($resp);
    }

    public function getIndividualWithoutDuplication(string $email)
    {
        $email = urlencode($email);
        $url = "IndividualSearch?filter.email={$email}";

        $collection = $this->callDBS($url);

        //dd($collection);
        $filtered = [];

        if (empty($collection)) {
            return false;
        } else {
            if (1 == count($collection)) {
                return $collection[0]->Id;
            } elseif (count($collection) > 1) {
                foreach ($collection as $item) {
                    //Mailing = adresse postal
                    if (false == $item->InvalidMailingAddress) {
                        $filtered[] = $item;
                    }
                }

                if (empty($filtered)) {
                    // multiple individuals with the same email but postal address not valid
                    // then I take the first one and continue (see with Admin if we want more filter like looking the created date... maybe check here if donation)
                    return $collection[0];
                }

                if (1 == count($filtered)) {
                    return $filtered[0];
                }

                $secondFiltered = [];

                if (count($filtered) > 1) {
                    $subscriptionList = ['BUL', 'CONT', 'AVI', 'FREEBUL', 'FREEAVI', 'PROMOBUL', 'MM'];
                    foreach ($filtered as $item) {
                        $url = "Individual/SubscriptionsAll/{$item->Id}";
                        $subscriptions = $this->callDBS($url);

                        foreach ($subscriptions as $subscription) {
                            if (in_array($subscription->Subscription->Name, $subscriptionList)) {
                                $secondFiltered[] = $item;
                                break;
                            }
                        }
                    }

                    if (empty($secondFiltered)) {
                        return $filtered[0];
                    }

                    if (1 == count($secondFiltered)) {
                        return $secondFiltered[0];
                    }

                    $lastFiltered = [];

                    if (count($secondFiltered) > 1) {
                        foreach ($secondFiltered as $item) {
                            $url = "Individual/Donations/{$item->Id}";
                            $donations = $this->callDBS($url);

                            if (false != $donations) {
                                $lastFiltered[] = $item;
                            }
                        }

                        //dd($lastFiltered);

                        if (empty($lastFiltered)) {
                            return $secondFiltered[0];
                        }

                        if (count($lastFiltered) >= 1) {
                            return $lastFiltered[0];
                        } else {
                            return false;
                        }
                    }
                }
                //dd($lastFiltered);
            } else {
                return false;
            }
        }
    }

    // add doc to the function avec ce qui et obligatoire
    public function createIndividual(array $fields)
    {
        $url = 'Individual';

        return $this->callDBS($url, true, $fields);
    }

    //TODO enlever abort (c'est du laravel pas du php, donc pas compatible avec toutes les app)
    //TODO garder les execptions, pas de 404. être précis dans les messages d'exceptions
    // Pour envoyer une exception specifique on peut créer une class avec un attribut pour le message.
    /**
     * Get the Id of an individual by his email.
     * @param string $email
     * @param bool $silent
     * @return string
     *
     */
    public function getIndividualId(string $email, $silent = false)
    {
        $arrayOfIndividuals = $this->getIndividuals($email);

        if (is_array($arrayOfIndividuals)) {
            if (count($arrayOfIndividuals) > 1) {
                //throw new \Exception("Many individuals match this email address");
                if ($silent) {
                    return false;
                } else {
                    abort(404, "L'adresse e-mail que vous souhaitez modifier est attachée à plusieurs fiches. Veuillez nous contacter à l'adresse contact@portesouvertes.fr");
                }
            }

            if (count($arrayOfIndividuals) < 1) {
                //throw new \Exception("No individual with this email address");
                if ($silent) {
                    return true;
                } else {
                    abort(404, "L'adresse e-mail à laquelle vous essayer d'accéder n'existe pas");
                }
            }
        } else {
            if ($silent) {
                return false;
            } else {
                abort(404, "Nous avons rencontré un problème lors du traitement de votre demande concernant l'e-mail {$email}. Veuillez nous contacter à l'adresse contact@portesouvertes.fr");
            }
        }

        $individual = $arrayOfIndividuals[0];

        return $individual->Id;
    }

    public function getIndividuals(string $email)
    {
        $email = urlencode($email);
        $url = "IndividualSearch?filter.email={$email}";

        //TODO passer en array simple collection c'est du laravel)
        $collection = ($this->callDBS($url));

        return $collection;
    }

    public function updateIndividual(string $email, array $fields)
    {
        $url = 'Individual/Post';

        return $this->callDBS($url, true, $fields);
    }

    public function getActiveIndividualId(string $email)
    {
        $arrayOfIndividuals = $this->getIndividuals($email);

        if (is_array($arrayOfIndividuals)) {
            return $arrayOfIndividuals;
        } else {
            return false;
        }
    }

// connection entité du CRM qui permet de lié une personne à un groupe (anthenne eglise)
    public function getConnectionsOfTheIndividual(string $individualID)
    {
        $url = "Individual/Connections/{$individualID}";

        return $this->callDBS($url);
    }

    /**
     * GROUPS.
     */
    public function findGroups(string $groupNameQuery)
    {
        $param = [
            'query' => '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false"> <entity name="account"> <attribute name="primarycontactid" /> <attribute name="telephone1" /> <attribute name="accountid" /> <attribute name="accountnumber" /> <attribute name="od_groupname" /> <attribute name="od_officialgroupname" /> <filter type="and"> <filter type="or"> <condition attribute="od_groupname" operator="like" value="%'.$groupNameQuery.'%" /> <condition attribute="od_officialgroupname" operator="like" value="%'.$groupNameQuery.'%" /> <condition attribute="address1_postalcode" operator="like" value="'.$groupNameQuery.'%" /> </filter> </filter> </entity> </fetch>',
        ];
        $url = 'Group/FetchXml';

        return $this->callDBS($url, true, $param);
    }

    public function findGroupsByZipCode(string $query)
    {
        $param = [
            'query' => '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false"> <entity name="account"> <attribute name="primarycontactid" /> <attribute name="telephone1" /> <attribute name="accountid" /> <attribute name="accountnumber" /> <attribute name="od_groupname" /> <attribute name="od_officialgroupname" /> <filter type="and"> <condition attribute="address1_postalcode" operator="like" value="'.$query.'%" /> <condition attribute="od_grouptype" operator="eq" value="100000000" /> <condition attribute="statecode" operator="eq" value="0" /> </filter> </entity> </fetch>',
        ];
        $url = 'Group/FetchXml';

        return $this->callDBS($url, true, $param);
    }

    public function connectGroupToIndividual(string $groupID, string $individualID)
    {
        $connections = $this->getConnectionsOfTheIndividual($individualID);
        $isConnected = false;

        if (!empty($connections)) {
            foreach ($connections as $oneConnection) {
                if ($oneConnection->ToId == $groupID) {
                    $isConnected = true;
                    break;
                }
            }
        }

        if (false == $isConnected) {
            $url = 'GroupToIndividualConnection';
            $connection = [
                'ConnectedFrom' => $groupID,
                'ConnectedTo' => $individualID,
                'RoleFrom' => 'Church',
                'RoleTo' => 'Church member',
                'Starting' => date('Y-m-d'),
            ];

            return $this->callDBS($url, true, $connection);
        } else {
            return false;
        }
    }

    public function getChurchById(string $groupID)
    {
        $url = "Group/Get/{$groupID}";

        return $this->callDBS($url);
    }

    /**
     * SUBSCRIPTIONS.
     */

    /**
     * @return mixed
     *
     * @throws \Exception
     */
    public function getIndividualSubscriptions(string $email)
    {
        $individualId = $this->getIndividualId($email, true);

        if ($individualId) {
            $url = "Individual/Subscriptions/{$individualId}";

            return $this->callDBS($url);
        } else {
            return false;
        }
    }

    public function getIndividualAllSubscriptionsById(string $individualId)
    {
        $url = "Individual/SubscriptionsAll/{$individualId}";

        return $this->callDBS($url);
    }

    // TylTAx reçus fiscaux ou non avec RF ou ERF
    //Le return false permet de savoir si on a eu besoin de faire le remove ou non. (on se place du coté de la function et non du crm). c'est une logique.

    public function removeTylTax(string $individualId)
    {
        $subsriptions = $this->getIndividualAllSubscriptionsById($individualId);
        foreach ($subsriptions as $subsription) {
            if ('TYL' == $subsription->Name) {
                $url = 'Subscriptions';

                return $this->callDBS($url, true, array_merge(get_object_vars($subsription), ['ProductId' => null, 'ProductName' => '']));
            }
        }

        return false;
    }

    public function getIndividualSubscriptionsById(string $individualId)
    {
        $url = "Individual/Subscriptions/{$individualId}";

        return $this->callDBS($url);
    }

    public function addSubscriptionToIndividual(string $individualId, array $subscriptions)
    {
        $url = "Individual/SubscriptionsAdd/{$individualId}";

        return $this->callDBS($url, true, $subscriptions);
    }

    public function setEndDateToSubscriptions(string $individualId, array $subscriptions)
    {
        $url = "Individual/SubscriptionsEnd/{$individualId}";

        return $this->callDBS($url, true, $subscriptions);
    }

    public function setEndDateToSubscription(string $individualId, array $subscriptions)
    {
        $url = "Individual/SubscriptionEnd/{$individualId}";

        return $this->callDBS($url, true, $subscriptions);
    }

    public function isSubscriberToFL(string $email)
    {
        $subscriptions = $this->getIndividualSubscriptions($email);

        if ($subscriptions) {
            //TODO mettre en constance ce GUID
            $fl = 'f4131bd6-6fbe-e511-9414-00155d0c180d';

            foreach ($subscriptions as $subscription) {
                if ($subscription->Subscription->Id == $fl) {
                    return true;
                    break;
                }
            }
        } else {
            return false;
        }
    }

    public function isSubscriberToNLCdN(string $email)
    {
        $subscriptions = $this->getIndividualSubscriptions($email);

        if ($subscriptions) {
            //TODO code laravel à mettre aussi ds un fichier de constantes
            $nl = Config::get('guids.nl.NLCDN');

            foreach ($subscriptions as $subscription) {
                if ($subscription->Subscription->Id == $nl) {
                    return true;
                    break;
                }
            }
        } else {
            return false;
        }
    }

    public function isSubscriberToPLNL(string $email)
    {
        $subscriptions = $this->getIndividualSubscriptions($email);

        if ($subscriptions) {
            $nl = Config::get('guids.nl.PLNL');

            foreach ($subscriptions as $subscription) {
                if ($subscription->Subscription->Id == $nl) {
                    return true;
                    break;
                }
            }
        } else {
            return false;
        }
    }

    /**
     * DONATIONS AND BATCHES.
     */


    // Non utilisé, l'ajout est mantenant automatique coté api crm.
    /**
     * @return bool|mixed
     */
    public function getTheLatestActiveBatch()
    {
        $url = 'Batch/GetOrCreateLatestActiveBatchFromWeb';

        return $this->callDBS($url);
    }


    public function addDonationToTheBatch(array $donation)
    {
        $url = 'Batch/AddDonationToLatestActiveBatchFromWeb';

        return $this->callDBS($url, true, $donation);
    }

    /**
     * ACTIVITIES.
     */

    /**
     * @return bool|mixed
     */
    public function addLetterActivity(string $indivualID, string $subject)
    {
        $url = "IndividualActivities/AddLetter/{$indivualID}";
        $activity = [
            'Subject' => $subject,
            'IndividualId' => $indivualID,
        ];

        return $this->callDBS($url, true, $activity);
    }

    /**
     * @return bool|mixed
     */
    public function addLetterActivityToGroup(string $groupID, string $subject)
    {
        $url = "GroupActivities/AddLetter/{$groupID}";
        $activity = [
            'Subject' => $subject,
            'GroupId' => $groupID,
        ];

        return $this->callDBS($url, true, $activity);
    }

    /**
     * MARKETING LISTS.
     */
    public function getMarketingList(string $id)
    {
        $url = "MarketingList/{$id}";

        return $this->callDBS($url);
    }

    /**
     * Get donation id based on baseReference
     */
    public function getDestinationId(string $baseReference)
    {
        $url = "Destination/Get";

        $destinations = $this->callDBS($url);

        foreach ($destinations as $item) {
            if($item->BaseReference === $baseReference) {
                return $item->Id;
            }
        }
        return false;
    }
}
